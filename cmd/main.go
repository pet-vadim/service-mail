package main

import (
	"gitlab.com/pet-vadim/libs/logger"
	"gitlab.com/pet-vadim/service-mail/internal/services/mailer"
	"gitlab.com/pet-vadim/service-mail/internal/transport/rabbitmq"
	"gitlab.com/pet-vadim/service-mail/internal/transport/smtp"
	"os"
	"os/signal"
	"strconv"
	"syscall"
)

func main() {
	logger.Info("Starting app")

	s := smtp.New(getSMTPConf())
	m := mailer.New(s)
	c := rabbitmq.NewConsumer(m, getRabbitMQConf())

	// Start
	go func() {
		c.StartConsume()
	}()

	// Listen for the interrupt signal.
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	logger.Info("received os.signal: " + (<-quit).String())

	if err := c.AMQPDialCh.Close(); err != nil {
		logger.Fatal("consumer channel close err: " + err.Error())
	}

	if err := c.AMQPDial.Close(); err != nil {
		logger.Fatal("consumer conn close err: " + err.Error())
	}

	s.Close()

	logger.Error("exit app")
}

func getRabbitMQConf() *rabbitmq.RabbitMq {
	return &rabbitmq.RabbitMq{
		DataSource: os.Getenv("BROKER_DATA_SOURCE"),
		QueueName:  os.Getenv("BROKER_QUEUE_NAME"),
		Durable:    toBool(os.Getenv("BROKER_DURABLE")),
		AutoDelete: toBool(os.Getenv("BROKER_AUTO_DELETE")),
		AutoAck:    toBool(os.Getenv("BROKER_AUTO_ACK")),
		Exclusive:  toBool(os.Getenv("BROKER_EXCLUSIVE")),
		NoLocal:    toBool(os.Getenv("BROKER_NO_LOCAL")),
		NoWait:     toBool(os.Getenv("BROKER_NO_WAIT")),
		Args:       nil,
	}
}

func getSMTPConf() *smtp.Config {
	return &smtp.Config{
		FromEmail:    os.Getenv("MAILER_FROM_EMAIL"),
		SMTP:         os.Getenv("MAILER_SMTP"),
		SMTPPort:     toInt(os.Getenv("MAILER_PORT")),
		SMTPUserName: os.Getenv("MAILER_USER_NAME"),
		SMTPPassword: os.Getenv("MAILER_PASSWORD"),
	}
}

func toBool(v string) bool {
	resBool, err := strconv.ParseBool(v)
	if err != nil {
		logger.Fatal("fail convert env to bool" + err.Error())
	}
	return resBool
}

func toInt(v string) int {
	SMTPPort, err := strconv.Atoi(v)
	if err != nil {
		logger.Fatal("incorrect SMTP_PORT env: " + v + " err: " + err.Error())
	}
	return SMTPPort
}

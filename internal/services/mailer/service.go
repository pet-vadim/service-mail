package mailer

import "context"

type Service interface {
	SendEmail(ctx context.Context, mail *Mail)
}

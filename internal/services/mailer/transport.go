package mailer

import "context"

type SMTP interface {
	Send(ctx context.Context, mail *Mail)
	Close()
}

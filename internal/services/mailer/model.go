package mailer

type Mail struct {
	ToEmail string `json:"to_email"`
	Subject string `json:"subject"`
	Msg     string `json:"msg"`
}

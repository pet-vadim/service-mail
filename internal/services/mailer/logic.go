package mailer

import (
	"context"
)

type srv struct {
	Mail SMTP
}

func New(s SMTP) Service {
	return &srv{Mail: s}
}

func (s *srv) SendEmail(ctx context.Context, mail *Mail) {
	s.Mail.Send(ctx, mail)
}

package rabbitmq

import (
	"context"
	"encoding/json"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/pet-vadim/libs/logger"
	"gitlab.com/pet-vadim/service-mail/internal/services/mailer"
	"time"
)

type Consumer struct {
	Mailer     mailer.Service
	AMQPDial   *amqp.Connection
	AMQPDialCh *amqp.Channel
	Stop       chan bool
	Conf       *RabbitMq
}

func NewConsumer(m mailer.Service, conf *RabbitMq) *Consumer {
	amqpDial, err := amqp.Dial(conf.DataSource)
	failOnError(err, "failed to connect to rabbitmq")

	ch, err := amqpDial.Channel()
	failOnError(err, "Failed to open a channel")

	_, err = ch.QueueDeclare(
		conf.QueueName,
		conf.Durable,
		conf.AutoDelete,
		conf.Exclusive,
		conf.NoWait,
		conf.Args,
	)
	failOnError(err, "failed to declare a queue")

	return &Consumer{
		Mailer:     m,
		AMQPDial:   amqpDial,
		AMQPDialCh: ch,
		Stop:       make(chan bool),
		Conf:       conf,
	}
}

func (c *Consumer) StartConsume() {
	err := c.AMQPDialCh.Qos(1, 0, false)
	failOnError(err, "failed to declare a queue")

	msgs, err := c.AMQPDialCh.Consume(
		c.Conf.QueueName,
		c.Conf.Consumer,
		c.Conf.AutoAck,
		c.Conf.Exclusive,
		c.Conf.NoLocal,
		c.Conf.NoWait,
		nil,
	)
	failOnError(err, "failed to register a consumer")

	go func() {
		logger.Info("starting listening messages")
		for d := range msgs {
			go c.consumeMsg(&d)
		}
	}()

	<-c.Stop
}

func (c *Consumer) consumeMsg(d *amqp.Delivery) {
	var mail mailer.Mail
	err := json.Unmarshal(d.Body, &mail)
	if err != nil {
		logger.Error("failed to unmarshal rabbitmq msg: " + err.Error())
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	c.Mailer.SendEmail(ctx, &mail)
	err = d.Ack(false)
	if err != nil {
		logger.Error("failed ack msg: " + err.Error())
	}
}

func failOnError(err error, msg string) {
	if err != nil {
		logger.Fatal(msg + " " + err.Error())
	}
}

type RabbitMq struct {
	DataSource  string
	QueueName   string
	Consumer    string
	Exchange    string
	DeliveryKey string
	Durable     bool
	AutoDelete  bool
	AutoAck     bool
	Exclusive   bool
	NoLocal     bool
	NoWait      bool
	Args        map[string]interface{}
}

package smtp

import (
	"context"
	"gitlab.com/pet-vadim/libs/logger"
	"gitlab.com/pet-vadim/service-mail/internal/services/mailer"
	"gopkg.in/gomail.v2"
)

type smtp struct {
	Conn   gomail.SendCloser
	Config *Config
}

func New(c *Config) mailer.SMTP {
	dialer := gomail.NewDialer(c.SMTP, c.SMTPPort, c.SMTPUserName, c.SMTPPassword)
	conn, err := dialer.Dial()
	if err != nil {
		logger.Fatal("can not connect to the smtp server: " + err.Error())
	}
	return &smtp{
		Conn:   conn,
		Config: c,
	}
}

func (s *smtp) Send(ctx context.Context, mail *mailer.Mail) {
	// Creating email
	m := gomail.NewMessage()
	m.SetHeader("From", s.Config.FromEmail)
	m.SetAddressHeader("Cc", mail.ToEmail, "")
	m.SetHeader("Subject", mail.Subject)
	m.SetBody("text/plain", mail.Msg)

	// Send email
	err := gomail.Send(s.Conn, m)
	if err != nil {
		logger.Error("first attempt sending email is fail")
		err = gomail.Send(s.Conn, m)
		if err != nil {
			logger.Error("second attempt sending email is fail, restart server")
		}
		return
	}
}

func (s *smtp) Close() {
	if err := s.Conn.Close(); err != nil {
		logger.Fatal("mailer connection close err: " + err.Error())
	}
}

type Config struct {
	FromEmail    string
	SMTP         string
	SMTPPort     int
	SMTPUserName string
	SMTPPassword string
}
